package cz.cvut.nemrtvej.wa2.hibernate;

import cz.cvut.nemrtvej.wa2.hibernate.entity.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.List;
import java.util.Properties;

public class Main {

    private static SessionFactory sessionFactory;

    public static void main(String[] args) {
        System.out.println("Hello world");

        configureHibernate();

        Person person = new Person();
        person.setFirstName("František");
        person.setLastName("Vomáčka");


        // save entity example
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(person);
        session.flush();
        transaction.commit();


        // load query exmple
        Person loadedPerson = (Person) session.get(Person.class, new Long(1));
        System.out.println(loadedPerson.getLastName());



        // hardcoded query example
        List<Person> persons = session.createQuery("from Person p").list();

    }

    private static void configureHibernate() {
        Configuration configuration = new Configuration();
        configuration.configure(); // load info from hibernate.cfg.xml  - or some given file..

        Properties properties = configuration.getProperties(); // parsed values from hibernate.cfg.xml

        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
        ServiceRegistry serviceRegistry = builder.applySettings(properties).build();
        sessionFactory = configuration.buildSessionFactory(); // umi generatovat Session ktera slouzi pro komunikaci s DB
    }
}
